function count() {
  let c = 0;

  function up() {
    c++;
    console.log(c);
  }
  return {
    up
  };
}

let counter = count();
counter.up();
counter.up();

console.log(counter.c);

let counetr = count();
counetr.up();
counetr.up();

class counterClass {
  c = 0;
  up() {
    this.c++;
    console.log(this.c)
  }
}

let counter2 = new counterClass();
let counter3 = new counterClass();
counter2.up();
counter2.up();

counter3.up();
counter3.up();
