// const obj = {};
// obj.nome = "victor";
// obj.endereco = "R do fodase";
// obj.nome = "bruna";
// obj.dizoi = function

// obj['nomecompleto'] = "bruna tiemi uchiha";

// const obj2 = new Object();
// obj2.nome = 'jemme';

// console.log(obj2);

// const query = [
//   {
//     nome: 'victor',
//     profissa: 'maiomeno'
//   },
//   {
//     nome: 'mauricio',
//     profissa: 'se acha'
//   },
//   {
//     nome: 'bruna',
//     profissa: 'para de me julgar'
//   }
// ];

const pessoa = {
  nome: "bispo",
  profissao: "cantor",
  dizeroi: function () {
    console.log("Fala fion");
  },
  qualmeunome: () => {
    console.log(this);
  },
};

const pessoa2 = {
  ...pessoa,
  profissao: "dev",
};

console.log(pessoa2);
// pessoa.qualmeunome();

const ar = [4, 2, 1, 0, 10, 2, 5, 9];
const ar2 = [];
ar.length;
ar.forEach((v) => ar2.push(v * 2));

const ar3 = ar.map((v) => v * 3);
const sum = ar.reduce((acc, cur) => {
  if (cur <= 5) return acc + cur;
  else return acc;
});
const fil = ar.filter((v) => {
  return v % 2 == 0;
});
const h = ar.some((v) => v > 10);
const h2 = ar.every((v) => v <= 10);
const f = ar.find((v) => v < 10);
const c = ar.concat(h, h2, f);

const c2 = [...ar, h, h2, ...c];

// console.log(c2);
