let a = 1;

function oi(x) { // void
  x * 1;
}

// Statement ou afirmação
function dois(x) {
  return x * 2;
}

// expression ou expressão
const tres = function (x) {
  return x * 3;
};

// arrow function ou googla ai otario
const shrek = (x) => x * 4;

console.log(shrek(3));
