// Tipos
/* Primitivos */
let a = "string";
let b = 1;
let c = false;
let d = null;
let e;

console.log(a, b, c, d, e);

// Tipos
// Complexos

let x = [0, 9, 3, 5, 8];

console.log(x[2]);
