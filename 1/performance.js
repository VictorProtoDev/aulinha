const ar = [];
for (let i = 0; i < 1000000; i++) ar.push(i);

console.time("for of");
for (el of ar) {
}
console.timeEnd("for of");

console.time("for");
for (let i = 0; i < ar.length; i++) {}
console.timeEnd("for");

console.time("foreach");
ar.forEach((v) => {});
console.timeEnd("foreach");

console.time("map");
ar.map((v) => {});
console.timeEnd("map");
