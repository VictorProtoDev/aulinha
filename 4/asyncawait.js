function pegarFruta(fruta) {
  return new Promise((res, rej) => {
    setTimeout(() => {
      const frutas = {
        laranja: "🍊",
        morango: "🍓",
        uva: "🍇",
      };
      if (frutas[fruta]) {
        res(frutas[fruta]);
      } else {
        rej("Fruta não encontrada");
      }
    }, 3000);
  });
}

async function smoothie() {
  const laranja = pegarFruta("laranja");
  const uva = pegarFruta("uva");
  const morango = pegarFruta("morango");
  const melancia = pegarFruta("melancia");

  try {
    const resultado = await Promise.all([morango, uva, laranja, melancia]);
    return resultado;
  } catch (err) {
    return err;
  }

  // return Promise.all([
  //   pegarFruta("laranja"),
  //   pegarFruta("uva"),
  //   pegarFruta("morango"),
  // ]);
}

console.log("pegar fruta");
smoothie().then(console.log);
console.log("pegando fruta");
