function pegarFruta(fruta) {
  return new Promise((res, rej) => {
    setTimeout(() => {
      const frutas = {
        laranja: "🍊",
        morango: "🍓",
        uva: "🍇",
      };
      if (frutas[fruta]) {
        res(frutas[fruta]);
      } else {
        rej("Fruta não encontrada");
      }
    }, 3000);
  });
};

const saladadefrutas = [];
console.log("Buscar fruta");

// Promises individuais
pegarFruta("laranja").then((fruta) => {
  saladadefrutas.push(fruta);
});

pegarFruta("uva").then((fruta) => {
  saladadefrutas.push(fruta);
});

pegarFruta("morango").then((fruta) => {
  saladadefrutas.push(fruta);
});

// Promise encadeada
pegarFruta("laranja")
  .then((fruta) => {
    console.log("achou laranja");
    saladadefrutas.push(fruta);
    return pegarFruta("uva");
  })
  .then((fruta) => {
    console.log("achou uva");
    saladadefrutas.push(fruta);
    return pegarFruta("morango");
  })
  .then((fruta) => {
    console.log("achou morango");
    saladadefrutas.push(fruta);
    console.log(saladadefrutas);
  })
  .catch((err) => {
    console.log("Erro! ", err);
  });

// callback hell
pegarFruta("laranja")
  .then((fruta) => {
    console.log("achou laranja");
    saladadefrutas.push(fruta);

    pegarFruta("uva").then((fruta) => {
      console.log("achou uva");
      saladadefrutas.push(fruta);

      pegarFruta("morango").then((fruta) => {
        console.log("achou morango");
        saladadefrutas.push(fruta);
        console.log(saladadefrutas);
      });
    });
  })
  .catch((err) => {
    console.log("Erro! ", err);
  });

// Promise.all
Promise.all([
  pegarFruta("laranja"),
  pegarFruta("uva"),
  pegarFruta("morango"),
]).then(([laranja, uva, morango]) => {
  console.log(uva, morango, laranja);
});

console.log("Buscando fruta");
