function pegarFruta(fruta, cb) {
  setTimeout(() => {
    const frutas = {
      laranja: "🍊",
      morango: "🍓",
      uva: "🍇",
    };
    return cb(frutas[fruta]);
  }, 1000);
}

console.time("programa");

pegarFruta("morango", (fruta) => console.log(fruta));

console.timeEnd("programa");
