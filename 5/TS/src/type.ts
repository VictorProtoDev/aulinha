let str: string;
let num: number;
let n: null;
let tuple: [string, number, boolean];
tuple = ["str", 10, true];

let an: any;
an = {};
an = 10;
an = true;
an = function () {};

let t: never[];
t = [];

// data casting
const text: any = "texto longo";
const tamanho = (text as string).length;

const num2: string = "26";
const num3 = Number(num2);

interface somador {
  readonly x: number;
  y: number;
  z?: number;
  [key: string]: any;
}

const a: somador = {
  x: 10,
  y: 100
};

a.y = 9;
// a.x = 1;

function pow3(param: somador) {
  // if(!param.z){
  //   return 0;
  // }

  return Math.pow(param.z || 0, param.y);
}

type newtipo = true | 'ruim' | 5;

export type match = '=' | '!=' | '>' | '<' | 'is' | 'is not' | 'like';
const aaaaaaaaa: newtipo = 5;
