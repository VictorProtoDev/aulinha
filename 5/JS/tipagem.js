function pow(x, y) {
  if (typeof x !== "number" || typeof y !== "number") {
    return 0;
  }
  return Math.pow(x, y);
}

// console.log(pow(5, 10));

function pow2(params) {
  if (typeof params.x !== "number" || typeof params.y !== "number") {
    return 0;
  }
  return Math.pow(params.x, params.y);
}

class PistaLavaRapidoHotwheels {
  constructor() {
    this.xiix = "vai encarar?!1";
  }
}

const tubarao = new PistaLavaRapidoHotwheels();
console.log(tubarao instanceof PistaLavaRapidoHotwheels);
