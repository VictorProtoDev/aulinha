class Pessoa {
  nome;

  idade;

  profissao;

  constructor({ nome, idade, profissao }) {
    this.nome = nome;
    this.idade = idade;
    this.profissao = profissao;
  }
}

class SuperHeroi extends Pessoa {
  nickname;

  constructor(params) {
    super(params);
    this.nickname = params.nickname;
  }

  usarPoder(poder) {
    console.log(this.nickname + " Usou " + poder);
  }
}

const params = {
  nome: "Bruna",
  idade: 22,
  profissao: "dev biscoiteira",
  nickname: "capopêra",
};
const person = new SuperHeroi(params);
person.usarPoder('👍')
