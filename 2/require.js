const prices = require("./prices.json");

const tot = prices.reduce(
  (acc, cur) => (cur.preco > 10 ? acc + cur.preco : acc),
  0
);

console.log(tot * 1.3);
